#!/usr/bin/env python3

from selenium import webdriver

# init chromedriver
option = webdriver.ChromeOptions()

# fullscreen
option.add_argument("--kiosk")

# keep window open when script terminates
option.add_experimental_option("detach", True)

# remove "Chrome is being controlled by automated test software" notification
option.add_experimental_option("excludeSwitches", ['enable-automation']);

# hide password manager
option.add_experimental_option('prefs', {
    'credentials_enable_service': False,
    'profile': {
        'password_manager_enabled': False
    }
})

# add cookie storage for rembering password and login
option.add_argument("user-data-dir=geforcenow_data/")

# driver path
ChromeDriverPath = "/usr/lib/chromium-browser/chromedriver"


def start_geforce_now():

    # Create new Instance of Chrome in incognito mode
    browser = webdriver.Chrome(ChromeDriverPath, options=option)

    url = 'https://play.geforcenow.com/mall/'

    # Go to desired website
    browser.get(url)


if  __name__ == "__main__":
	start_geforce_now()
