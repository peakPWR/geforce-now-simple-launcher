# GeForce NOW Simple Launcher

This is a python script using the awesome [selenium python webdriver](https://selenium-python.readthedocs.io/).

- It launches a chromium webdriver opening the Geforce Now Website in fullscreen mode.
- It's only necessary to login once, as the login is saved in local cookies located in the newly created directory "geforcenow_data".

This script uses selenium python bindings for the web operation.

Trademark names are only used to discribe the function of this script.
